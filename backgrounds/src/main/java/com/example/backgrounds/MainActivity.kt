package com.example.backgrounds

import androidx.fragment.app.FragmentActivity

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : FragmentActivity(R.layout.main_activity)
