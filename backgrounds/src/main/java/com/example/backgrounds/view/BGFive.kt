package com.example.backgrounds.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.backgrounds.R
import com.example.backgrounds.ui.theme.FragPracticeTheme

/**
 * Background five.
 *
 * @constructor Create empty B g five
 */
class BGFive : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {
            // Dispose of the Composition when the view's LifecycleOwner is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                FragPracticeTheme() {
                    // In Compose world
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color.Red)
                    ) {
                        Button(onClick = { findNavController().navigate(R.id.BGFour) }) {
                            Text(text = "Previous")
                        }
                        Button(onClick = {
                            findNavController().popBackStack(R.id.BGOne, true)
                        }) {
                            Text(text = "Close")
                        }
                    }
                }
            }
        }
    }
}
