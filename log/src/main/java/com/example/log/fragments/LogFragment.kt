package com.example.log.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment

/**
 * Log fragment.
 *
 * @constructor Create empty Log fragment
 */
class LogFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(null, "onCreate()")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {
            Log.d(null, "onCreateView()")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(null, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        Log.d(null, "onStart")
        super.onStart()
    }

    override fun onResume() {
        Log.d(null, "onResume")
        super.onResume()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        Log.d(null, "onViewStateRestored")
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onPause() {
        Log.d(null, "onPause")
        super.onPause()
    }

    override fun onStop() {
        Log.d(null, "onStop")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(null, "onDestroy")
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        Log.d(null, "onSaveInstanceState")
        super.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        Log.d(null, "onDestroyView")
        super.onDestroyView()
    }
}
