package com.example.fragpractice

import androidx.fragment.app.FragmentActivity

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : FragmentActivity(R.layout.main_activity)

/*
FragmentPOCTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = "fragment") {
                        composable("fragment") {
                            VibrantFragment()
                        }
                    }
                }
            }
 */
