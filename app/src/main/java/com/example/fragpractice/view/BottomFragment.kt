package com.example.fragpractice.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import com.example.fragpractice.ui.theme.FragPracticeTheme

/**
 * Bottom fragment.
 *
 * @constructor Create empty Bottom fragment
 */
class BottomFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {
            // Dispose of the Composition when the view's LifecycleOwner is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                FragPracticeTheme {
                    // In Compose world
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color.Blue)
                    ) {}
                    // To navigate forward
                    // Activity to activity -> Intent(this, ActivityToGoTo:: Class.java
                    // Frag to frag -> findNavController().navigate()R.id.DestinationId)

                    // To go back
                    // Activity -> finish()
                    // Fragment(with nav) findNavController().navigateUp()
                }
            }
        }
    }
}
